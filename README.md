## Dataset app with Laravel

## Installation:
Projects uses laravel 8.75, Vue.js 3 and has below requirements:
* Php 7.4
* Node.js version 18 (recommended)
* Installed docker and docker compose on ubuntu
* Configs for docker containers are inside `infra` folder

Before installation please get your user name and uid from you machine by running:
* For getting your user name run
```shell
whoami
```

* For getting your UID (usually it is 1000) run:
```shell
echo $UID
```

* Open `docker-compose.yml` file and change user: `vagrant` to your user name and uid to your UID. You can also change ports if you need
* Copy .env.example file and create .env file
* Open .env file and change APP_URL if you need
* Run below commands
```shell
composer install
npm install
```

Run for starting docker
```shell
sudo docker compose up -d #wait until `mysql container` is healthy
sudo docker compose exec app php artisan migrate
sudo docker compose exec app php artisan dataset:import dataset.csv #Wait until data is imported
```

* Open in browser: http://localhost:8000 (or the APP_URL you set in .env file)

### Information about import and export
#### Import
In order to import data there is `php artisan dataset:import {filename}` command. Application is created in SOLID way. So in the future you can add new importer. For example xml importer, pdf importer etc. 
In order to add new importer you need to add it to `fileParsers` array inside `config/app.php` file

If you want importer for other models/tables then you can create them inside `app/Services/Importer/` folder and extend `BaseImporter.php` class

#### Export
Classes responsible for export are inside `app/Services/Exporter` folder. At the moment we have `ClientsExporter` and `CsvExport` classes. This part is also made in SOLID way. 
So in the future you can add for example new `XmlExporter` and then add it to `fileExporters` array inside `config/app.php` file.
At the moment we have `ClientsExporter` for clients table. In the future you can add new exporters.
For example `UsersExporter` for `users` table by extending `BaseExporter` class

### Notes
As you see I implemented direct csv download feature in this project. I thought that you need to this implementation.
But there is also another way for exporting csv. Instead of direct download (which can do highload for server) it is better to create exports table, save csv file in storage and add path to exports table and download it whenever we want.
