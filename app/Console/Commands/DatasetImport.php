<?php

namespace App\Console\Commands;

use App\Models\Client;
use App\Services\Importer\Clients\ClientsImporter;
use Illuminate\Console\Command;
use Symfony\Component\Console\Command\Command as CommandAlias;

class DatasetImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dataset:import {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import dataset';

    /**
     * File parsers
     * @var array
     */
    protected $fileParsers = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->fileParsers = config('app.fileParsers');

    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $filename = $this->argument('filename');
        $clientsImporter = new ClientsImporter($filename);

        try {
            $clientsImporter->import();
            $data = $clientsImporter->getData();
            $clientsImporter->transformData();

            $this->output->info('Starting to import data from '.$filename);
            $count = count($data);
            $this->output->progressStart($count);
            $i = $count;
            foreach ($data as $client) {
                $client = new Client($client);
                $client->save();
                $this->output->progressAdvance();
                $i--;
            }
            $this->output->progressFinish();
            $this->output->success('Data successfully imported');
            return CommandAlias::SUCCESS;
        } catch (\Exception $ex) {
            $this->error($ex->getMessage());
            return CommandAlias::FAILURE;
        }

    }
}
