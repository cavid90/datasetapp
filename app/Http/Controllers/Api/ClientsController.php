<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Services\Exporter\Clients\ClientsExporter;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    /**
     * Get all clients.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $clientsModel = new Client();
        $columns = $clientsModel->getColumns();
        $categories = $clientsModel->getCategories();
        return response()->json(['columns' => $columns, 'categories' => $categories]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse | \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function search(Request $request)
    {
        $clientsModel = new Client();
        $get_csv = $request->get('csv');
        $columns = (empty($request->get('columns')) || !$request->has('columns')) ? $clientsModel->getColumns() : $request->get('columns');
        if(!is_array($columns)) {
            $columns = explode(',', $columns);
        }
        $clients = $clientsModel->searchWithPagination($columns);
        if($get_csv) {
            $table_name = $clientsModel->getTable();
            $clientsExporter = new ClientsExporter($table_name, 'csv');
            return $clientsExporter->export($columns, $clients);
        }
        return response()->json($clients);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        //
    }
}
