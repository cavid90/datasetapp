<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class Client extends Model
{
    use HasFactory;

    /**
     * Fillable fields
     * @var string[]
     */
    protected $fillable = [
        'category',
        'firstname',
        'lastname',
        'email',
        'gender',
        'birthDate'
    ];

    /**
     * Disable
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get categories by grouping data
     * @return array
     */
    public function getCategories()
    {
        return self::query()->groupBy('category')->get('category')->map(function ($row) {
            return $row['category'];
        })->toArray();
    }

    /**
     * Get columns of table
     * @return array
     */
    public function getColumns()
    {
        return Schema::getColumnListing($this->getTable());
    }

    /**
     * Get all data with pagination
     * @param int $per_page
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAllWithPagination(int $per_page = 10)
    {
        return self::query()->limit(100)->paginate($per_page);
    }

    /**
     * Search by get data
     * @param $columns
     * @param int $per_page
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function searchWithPagination($columns, int $per_page = 10)
    {
        $query = self::query();
        if(\request()->has('per_page') && !empty(\request()->get('per_page')) && intval(\request('per_page')) > 0) {
            $per_page = \request()->get('per_page');
        }

        if(\request()->has('category') && !empty(\request('category'))) {
            $query->where('category', '=', \request('category'));
        }

        if(\request()->has('firstname') && !empty(\request('firstname')) && strlen(\request('firstname')) >= 2) {
            $query->where('firstname', 'LIKE', '%'.\request('firstname').'%');
        }

        if(\request()->has('gender') && !empty(\request('gender'))) {
            $gender = is_string(\request('gender')) ? explode(',', \request('gender')) : \request('gender');
            $query->whereIn('gender', $gender);
        }

        if(\request()->has('birthDate') && !empty(\request('birthDate'))) {
            $query->where('birthDate', '=', \request('birthDate'));
        }

        if(\request()->has('min_age') && !empty(\request('min_age')) && \request()->has('max_age') && !empty(\request('max_age'))) {
            $from = Carbon::today()->subYears(\request('min_age'));
            $to = Carbon::today()->subYears(\request('max_age'));
            $query->whereBetween('birthDate', [$to, $from]);
        }

        $data = (\request()->has('csv') && boolval(\request('csv')) === true) ? $query->get($columns) : $query->paginate($per_page, $columns);
        return $data;

    }
}
