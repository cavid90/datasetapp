<?php

namespace App\Services\Exporter;

abstract class BaseExporter
{
    /**
     * Exporters list
     * @var array
     */
    protected $exporters = [];

    /**
     * Type of file
     * @var string
     */
    protected $fileType;

    /**
     * Construct and set exporter
     */
    public function __construct()
    {
        $exporters = config('app.fileExporters');
        foreach ($exporters as $ext => $exporter) {
            $this->exporters[$ext] = app()->make($exporter);
        }
    }

    /**
     * Export method which should be implemented in child class
     * @return mixed
     */
    abstract public function export($columns, $data);

    /**
     * @param $table_name
     * @return string
     */
    protected function makeFileName($table_name)
    {
        return $table_name.'-'.date('Y-m-d_H-i').'.'.$this->fileType;
    }
}
