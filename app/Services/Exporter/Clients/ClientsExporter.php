<?php

namespace App\Services\Exporter\Clients;

use App\Services\Exporter\BaseExporter;
use App\Services\Exporter\IExporter;

class ClientsExporter extends BaseExporter
{
    /**
     * @var string
     */
    protected $fileNamePrefix = '';

    /**
     * @var IExporter
     */
    protected $exporter;

    public function __construct($prefix, $type = 'csv')
    {
        parent::__construct();
        $this->fileNamePrefix = $prefix;
        $this->exporter = $this->exporters[$type];
        $this->fileType = $type;
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function export($columns, $data)
    {
        $file_name = $this->makeFileName($this->fileNamePrefix);
        return $this->exporter->export($columns, $data, $file_name);
    }

}
