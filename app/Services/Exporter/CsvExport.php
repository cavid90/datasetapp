<?php

namespace App\Services\Exporter;

class CsvExport implements IExporter
{
    /**
     * Method to export data
     * @param $data
     * @param $fileName
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function export($columns, $data, $fileName) {
        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $callback = function() use($data, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns, ';');

            foreach ($data as $d) {
                foreach ($columns as $column) {
                    $row[$column] = $d->$column;
                }

                fputcsv($file, $row, ';');
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
