<?php

namespace App\Services\Exporter;

interface IExporter
{
    public function export($columns, $data, $fileName);
}
