<?php

namespace App\Services\FileParser;

abstract class BaseParser
{
    /**
     * Parsed data
     * @var array
     */
    protected $data;

    /**
     * Array of columns of parsed data
     * @var array
     */
    protected $columns;

    /**
     * Parse file to array
     * @param string $filename
     * @return mixed
     */
    abstract function parse(string $filename);

    /**
     * Get data
     * @return array
     */
    public function getParsedData(): array
    {
        return $this->data;
    }

    /**
     * Set array of columns for data
     * @param $line
     * @return void
     */
    public function setColumns($line) {
        $this->columns = $line;
    }

    /**
     * Get array of columns of data
     * @return array
     */
    public function getColumns() {
        return $this->columns;
    }
}
