<?php

namespace App\Services\FileParser;

use Exception;

class CsvParser extends BaseParser
{
    /**
     * Read csv file and make array from its lines
     * @param string $filename
     * @param string $delimiter
     * @return void
     * @throws Exception
     */
    public function parse(string $filename, string $delimiter = ',') {
        if(!is_file(app()->basePath(env('CSV_FILE_DIR').$filename))) {
            throw new Exception('File not found');
        }
        $file = fopen(app()->basePath(env('CSV_FILE_DIR').$filename), 'r');

        $i = 0;
        while(($line = fgetcsv($file, 0, $delimiter)) !== false) {
            if($i > 0) {
                $columns = $this->getColumns();
                $this->data[] = array_combine($columns, $line);
            } else {
                $this->setColumns($line);
            }
            $i++;
        }
    }
}
