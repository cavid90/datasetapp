<?php

namespace App\Services\Importer;

abstract class BaseImporter
{
    /**
     * Array of file parsers. At the moment we have Csv parser, in the future may be xml parser
     * @var array|\Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    protected $fileParsers = [];

    /**
     * Name of file which will be read
     * @var
     */
    protected $filename;

    public function __construct($filename)
    {
        $this->fileParsers = config('app.fileParsers');
        $this->filename = $filename;
    }

    /**
     * Get file extension in order to be able to use related parser (Csv, Xml, etc.)
     * @return false|mixed|string
     */
    protected function getExtension() {
        $array = explode('.', $this->filename);
        return end($array);
    }

    /**
     * Abstract method import
     * @return mixed
     */
    abstract public function import();

    /**
     * Abstract method get imported data
     * @return mixed
     */
    abstract public function getData();

}
