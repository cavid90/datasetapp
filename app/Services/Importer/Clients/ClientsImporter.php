<?php

namespace App\Services\Importer\Clients;

use App\Services\Importer\BaseImporter;

class ClientsImporter extends BaseImporter
{
    /**
     * Imported data
     * @var array
     */
    protected $data = [];

    /**
     * Data which is transformed to class
     * @var array
     */
    protected $transformedData = [];

    /**
     * Constructor
     * @param $filename
     */
    public function __construct($filename)
    {
        parent::__construct($filename);
    }

    /**
     * Import method
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function import(): void
    {
        $ext = $this->getExtension();
        $parser = app()->make($this->fileParsers[$ext]);
        $parser->parse($this->filename);
        $this->data = $parser->getParsedData();
        $this->transformData();
    }

    /**
     * Get imported data
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Transform data to class
     * @return void
     */
    public function transformData():void {
        foreach ($this->data as $d) {
            $newData = new ClientsTransformer($d);
            //print_r($newData);exit;
            $this->transformedData[] = $newData;
        }
    }

    /**
     * Get transformed data
     * @return IClientsStructure[]
     */
    public function getTransformedData(): array
    {
        return $this->transformedData;
    }
}
