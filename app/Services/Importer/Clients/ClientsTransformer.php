<?php

namespace App\Services\Importer\Clients;

class ClientsTransformer implements IClientsStructure
{
    /**
     * Array of transactions
     * @var array
     */
    protected $data;

    /**
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get date from array
     * @return string
     */
    public function getCategory():string
    {
        return $this->data['category'];
    }

    /**
     * Get user id from array
     * @return string
     */
    public function getFirstname():string
    {
        return $this->data['firstname'];
    }

    /**
     * Get user type from array
     * @return string
     */
    public function getLastname():string
    {
        return $this->data['lastname'];
    }

    /**
     * Get operation type from array
     * @return string
     */
    public function getEmail():string
    {
        return $this->data['email'];
    }

    /**
     * Get gender from array
     * @return string
     */
    public function getGender(): string
    {
        return $this->data['gender'];
    }

    /**
     * Get currency from array
     * @return string
     */
    public function getBirthDate():string
    {
        return $this->data['birthDate'];
    }
}
