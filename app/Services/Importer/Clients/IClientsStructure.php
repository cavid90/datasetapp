<?php

namespace App\Services\Importer\Clients;

interface IClientsStructure
{
    public function getCategory():string;
    public function getFirstname():string;
    public function getLastname():string;
    public function getEmail():string;
    public function getGender():string;
    public function getBirthDate():string;
}
