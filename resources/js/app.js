require('./bootstrap');
import { createApp } from 'vue';
import router from './router'
import ClientIndex from './components/clients/ClientIndex.vue';

const app = createApp({
    components: {
        ClientIndex
    }
});
app.use(router);
router.isReady().then(() => {
    app.mount('#app');
})
