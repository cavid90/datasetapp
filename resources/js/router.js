import { createRouter, createWebHistory } from 'vue-router'

import ClientIndex from './components/clients/ClientIndex';

const routes = [
    {
        path: '/',
        name: 'client-index',
        component: ClientIndex
    }
];

export default createRouter({
    history: createWebHistory(),
    routes
})
